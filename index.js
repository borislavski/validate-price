let div = document.createElement('div');
document.body.append(div);
div.classList.add('form');

let label = document.createElement('label');
label.innerText = 'Price';
div.append(label);

let input = document.createElement('input');
input.setAttribute('type', 'number');
input.style.border = '1px solid black';
div.append(input);

let currentValueWrapElem = document.createElement('span');
let currentValueElem = document.createElement('span');
let buttonElem = document.createElement('button');
let error = document.createElement('div');

currentValueWrapElem.innerText = 'Текущая цена:';
currentValueWrapElem.classList.add('current-value-wrap');
div.append(currentValueWrapElem);

currentValueElem.classList.add('current-value');
currentValueElem.style.color = 'green';

error.innerText = 'Please enter correct price';
error.classList.add('error');
error.style.color = 'red';

buttonElem.innerText = 'x';

input.addEventListener('focus', function (event) {
    event.target.style.outline = 'none';
    event.target.style.border = '1px solid green';
});

input.addEventListener('focusout', function (event) {
    event.target.style.border = '1px solid black';
    currentValueElem.innerHTML = event.target.value;
    const errorVal = event.target.parentNode.querySelector('.error');
    const currentValueWrap = event.target.parentNode.querySelector('.current-value-wrap');
    const currentValue = event.target.parentNode.querySelector('.current-value');
    const btn = event.target.parentNode.querySelector('button');

    if (event.target.value < 0) {
        event.target.style.border = '1px solid red';
        event.target.parentNode.append(error);

        if (currentValue !== null) {
            currentValue.remove()
        }

        if (btn !== null) {
            btn.remove()
        }

    } else {
        currentValueWrap.append(currentValueElem);
        currentValueWrap.append(buttonElem);

        if (errorVal !== null) {
            errorVal.remove();
        }
    }
});

buttonElem.addEventListener('click', function (event) {
    event.target.closest('.form').querySelector('input').value = '';
    event.target.parentNode.querySelector('.current-value').remove();
    event.target.remove();

});


